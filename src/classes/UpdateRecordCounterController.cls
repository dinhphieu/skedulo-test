public class UpdateRecordCounterController {

    @AuraEnabled
    public static Boolean increment(Id recordId, String recordType) {
        switch on recordType {
            when 'Account' {
                Account record = [SELECT Id, Counter__c FROM Account WHERE Id = :recordId];
                record.Counter__c = coerceNullToZero(record.Counter__c) + 1;

                update record;

                return true;
            }
            when 'Contact' {
                Contact record = [SELECT Id, Counter__c FROM Contact WHERE Id = :recordId];
                record.Counter__c = coerceNullToZero(record.Counter__c) + 1;

                update record;

                return true;
            }
        }

        return false;
    }

    private static Decimal coerceNullToZero(Decimal n) {
        return n == null ? 0 : n;
    }
}