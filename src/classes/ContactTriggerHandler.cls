public class ContactTriggerHandler {

    public static void afterUpdate() {
        Set<Id> parentAccountIds = new Set<Id>();

        for (Contact c : (List<Contact>) Trigger.new) {
            parentAccountIds.add(c.AccountId);
        }

        AccountRollupHelper.fromAccountIds(parentAccountIds).sumTotalActiveContacts().process();
    }
}