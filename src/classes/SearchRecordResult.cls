public class SearchRecordResult {
    @AuraEnabled
    public Id Id;
    @AuraEnabled
    public String RecordType;
    @AuraEnabled
    public String Name;
    @AuraEnabled
    public Decimal Counter;

    public SearchRecordResult(Id id, String recordType, String name, Decimal counter) {
        this.Id = id;
        this.RecordType = recordType;
        this.Name = name;
        this.Counter = counter;
    }
}