@RestResource(UrlMapping='/api/v1/contacts/*')
global class ContactAPIService {

    /**
     * @return Response
     * @example
     * curl --location --request GET 'https://skedulo-e-dev-ed.my.salesforce.com/services/apexrest/api/v1/contacts' \
        --header 'Authorization: Bearer ACCESS_TOKEN'
     */
    @HttpGet
    global static String handleGet() {
        return 'Hi there!';
    }

    /**
     * @example
     * curl --location --request POST 'https://skedulo-e-dev-ed.my.salesforce.com/services/apexrest/api/v1/contacts' \
        --header 'Authorization: Bearer ACCESS_TOKEN' \
        --header 'Content-Type: application/json' \
        --data-raw '[
            {
                "Id": "0035g000003jsZ4AAI",
                "Counter__c": 45,
                "Description": "Nice description"
            },
            {
                "Id": "0035g000003jsZ3AAI",
                "Counter__c": 100,
                "AssistantName": "John Doe"
            }
        ]'
     */
    @HttpPost
    global static void handlePost() {
        ContactAPIController.handlePost();
    }
}