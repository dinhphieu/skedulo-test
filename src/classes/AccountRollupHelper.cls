public class AccountRollupHelper {

    private Map<Id, Account> accounts = new Map<Id, Account>();
    private Set<Account> accountsToUpdate = new Set<Account>();

    private AccountRollupHelper(Set<Id> accountIds) {
        this.accounts = new Map<Id, Account>([SELECT Total_Contacts__c, (SELECT Active__c FROM Contacts) FROM Account WHERE Id IN :accountIds]);
    }

    public static AccountRollupHelper fromAccountIds(Set<Id> accountIds) {
        return new AccountRollupHelper(accountIds);
    }

    public AccountRollupHelper sumTotalActiveContacts() {
        for (Account a : accounts.values()) {
            // Calculate active contacts
            List<Contact> activeContactsCalc = calcActiveContacts(a.Contacts);

            // Compare the Account previous total contacts value with the newly calculated one
            if (a.Total_Contacts__c != activeContactsCalc.size()) {
                a.Total_Contacts__c = activeContactsCalc.size();

                // Add Account for update
                this.accountsToUpdate.add(a);
            }
        }

        return this;
    }

    public void process() {
        // Remove duplicates
        List<Account> accountsToUpdateNoDupes = new List<Account>(new Set<Account>(this.accountsToUpdate));

        update accountsToUpdateNoDupes;
    }

    private static List<Contact> calcActiveContacts(List<Contact> contacts) {
        List<Contact> activeContacts = new List<Contact>();

        for (Contact c : contacts) {
            if (c.Active__c) {
                activeContacts.add(c);
            }
        }

        return activeContacts;
    }
}