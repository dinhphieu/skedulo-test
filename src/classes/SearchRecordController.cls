public class SearchRecordController {

    @AuraEnabled(Cacheable=true)
    public static List<SearchRecordResult> search(String searchTerm) {
        // Skip if search term length is less than 2 chars, min 2 are needed for running SOSL queries
        if (searchTerm.length() < 2) {
            return new List<SearchRecordResult>();
        }

        // Prepare query params
        String wildcardST = '%' + searchTerm + '%';
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [
                FIND :searchTerm
                IN NAME FIELDS
                        RETURNING
                        Account(Id, Name, Counter__c WHERE Name LIKE :wildcardST ORDER BY Name),
                        Contact(Id, Name, Counter__c WHERE Name LIKE :wildcardST ORDER BY Name)
        ];

        // Prepare results
        List<SearchRecordResult> results = new List<SearchRecordResult>();

        // Extract Accounts & convert them into SearchRecordResult
        List<Account> accounts = (List<Account>) searchResults[0];
        for (Account account : accounts) {
            results.add(new SearchRecordResult(account.Id, 'Account', account.Name, account.Counter__c));
        }

        // Extract Contacts & convert them into SearchRecordResult
        List<Contact> contacts = (List<Contact>) searchResults[1];
        for (Contact contact : contacts) {
            results.add(new SearchRecordResult(contact.Id, 'Contact', contact.Name, contact.Counter__c));
        }

        return results;
    }
}