public class ContactAPIController {

    /**
     * Responses based on Google JSON guide: https://google.github.io/styleguide/jsoncstyleguide.xml
     */
    public static void handlePost() {
        Blob payload = RestContext.request.requestBody;
        RestContext.response.addHeader('Content-Type', 'application/json');

        try {
            // Strictly deserialize the payload into a list of Contact and throw a JSONException in case any field name is malformed
            List<Contact> contacts = (List<Contact>) JSON.deserializeStrict(payload.toString(), List<Contact>.class);

            // Attempt to update the contacts and throw a DmlException in case any field value's primitive type is not respected
            update contacts;

            // Prepare record IDs that got successfully updated
            List<Map<String, Object>> successIds = new List<Map<String, Object>>();

            // Collect the IDs and convert them into a list of objects
            for (Id contactId : (new Map<Id, Contact>(contacts)).keySet()) {
                successIds.add(new Map<String, Object>{
                        'id' => contactId
                });
            }

            // Send the array of objects with IDs
            RestContext.response.statusCode = 200;
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new Map<String, Object>{
                    'data' => successIds
            }));
        } catch (JSONException e) {
            // Send the default JSONException error
            RestContext.response.statusCode = 400;
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new Map<String, Object>{
                    'error' => new Map<String, Object>{
                            'code' => 400,
                            'message' => 'Error: failed to parse JSON from request. ' + e.getMessage()
                    }
            }));
        } catch (DmlException e) {
            // Prepare the record IDs that were not found
            List<String> notFoundIds = new List<String>();

            // Loop through all the DMLs and retrieve the IDs
            for (Integer i = 0; i < e.getNumDml(); i++) {
                if (e.getDmlType(i) == StatusCode.MALFORMED_ID) {
                    notFoundIds.add(e.getDmlId(i));
                }
            }

            // Send a custom error with the list of IDs
            if (notFoundIds.size() > 0) {
                String notFoundIdsString = String.join(notFoundIds, ', ');

                RestContext.response.statusCode = 422;
                RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new Map<String, Object>{
                        'error' => new Map<String, Object>{
                                'code' => 422,
                                'message' => 'Error: failed to update. The following record IDs do not exist: ' + notFoundIdsString
                        }
                }));

                return;
            }

            // Send the default DmlException error
            RestContext.response.statusCode = 422;
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new Map<String, Object>{
                    'error' => new Map<String, Object>{
                            'code' => 422,
                            'message' => 'Error: failed to process JSON from request. ' + e.getMessage()
                    }
            }));
        } catch (Exception e) {
            // Send the other remaining unhandled exceptions
            RestContext.response.statusCode = 500;
            RestContext.response.responseBody = Blob.valueOf(JSON.serialize(new Map<String, Object>{
                    'error' => new Map<String, Object>{
                            'code' => 500,
                            'message' => e.getMessage()
                    }
            }));
        }
    }
}