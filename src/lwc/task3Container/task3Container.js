import {LightningElement, track} from 'lwc';

export default class Task3Container extends LightningElement {
    @track searchInput = '';
    @track formData = {};

    handleInputChange(event){
        this.searchInput = event.target.value;
        this.formData[event.target.name] = event.target.value;
    }
}