import {LightningElement, api, wire} from 'lwc';
import getRecords from "@salesforce/apex/SearchRecordController.search";

const columns = [
    {label: 'Record Name', fieldName: 'Name', type: 'text'},
    {label: 'Record Type', fieldName: 'RecordType', type: 'text'},
    {label: 'Click to open', fieldName: 'Url', type: 'url', typeAttributes: {label: 'View', target: '_blank'}}
];

export default class Task2Datatable extends LightningElement {
    results = [];
    columns = columns;

    @api searchInput;

    @wire(getRecords, {searchTerm: '$searchInput'})
    wiredResults({error, data}) {
        if (error) {
            console.log(error);
        } else if (data) {
            this.results = data.map(row => {
                // Add Url hyperlink property based from Id
                const Url = `/${row.Id}`;
                return {...row , Url}
            });
        }
    }
}