import {LightningElement, api, wire} from 'lwc';
import getRecords from "@salesforce/apex/SearchRecordController.search";
import incrementRecordCounter from "@salesforce/apex/UpdateRecordCounterController.increment";
import {refreshApex} from '@salesforce/apex';

const columns = [
    {label: 'Record Name', fieldName: 'Name', type: 'text'},
    {label: 'Record Type', fieldName: 'RecordType', type: 'text'},
    {label: 'Counter', fieldName: 'Counter', type: 'text'},
    {
        label: 'Click to update counter',
        type: 'button',
        typeAttributes: {label: 'Increment Counter', name: 'increment_counter_button', variant: 'base'}
    }
];

export default class Task2Datatable extends LightningElement {
    results = [];
    columns = columns;

    @api searchInput;

    wiredGetRecord;

    @wire(getRecords, {searchTerm: '$searchInput'})
    wiredResults(response) {
        this.wiredGetRecord = response;
        const {error, data} = response;

        if (error) {
            console.error(error);
        } else if (data) {
            this.results = data.map(row => {
                // Coerce Counter to 0 in case it's blank
                const Counter = row.Counter === undefined ? 0 : row.Counter;
                return {...row, Counter}
            });
        }
    }

    handleRowAction(event) {
        const recordId = event.detail.row.Id;
        const recordType = event.detail.row.RecordType;

        incrementRecordCounter({recordId, recordType}).then(result => {
            return refreshApex(this.wiredGetRecord);
        }).catch(error => {
            console.error(error);
        });
    }
}